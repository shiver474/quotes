## French Loafing
```
<+grahamedgecombe> someone has cut the end off of a baguette   
<+grahamedgecombe> scooped out all the bread on the inside  
<+grahamedgecombe> and just left the crust in the kitchen   
```

## Three-Quarters Disappointment
```
<+grahamedgecombe> something annoying is that someone seems to break the last cookie in a bag into quarters  
<+grahamedgecombe> take one quarter  
<+grahamedgecombe> and leave three quarters in the bag  
<+grahamedgecombe> thus disappointing the next person  
```
## Tartan Paint
```
< xae> radio operator got a new guy to get a 'fog sample'
< xae> told him they didnt know if the helicopter could fly, needed to know precisely how thick the fog was
< xae> so gave him a jar and told him to go catch some fog
< xae> que this retard standing on the helideck with a jar trying to quite literally catch fog
```
